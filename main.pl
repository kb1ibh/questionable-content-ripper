use LWP::Simple;
use LWP::UserAgent;
use HTTP::Request;

$ua = LWP::UserAgent->new;
$n = 1720;
while(1)
{
	$response = $ua->get("http://questionablecontent.net/comics/$n.png");
	
	if($response->code() == "404")
	{
		die("Died at $n");
	}
	$ua->mirror("http://questionablecontent.net/comics/$n.png", "C:\\QC\\$n.png");
	print("Downloaded Number $n\n");
	$n++;
}

